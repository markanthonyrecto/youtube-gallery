'use strict';
var bp = {
	width: 0,
	height: 0,
	isMobile: false,
	init: function() {
		bp.mobileDetect();
		// for mobile
		if (bp.isMobile) {
			document.querySelector('html').classList.add('bp-touch');
		}

		bp.resize();
	},
	ready: function() {
		bp.resize();
	},
	resizeTimer: 0,
	resize: function() {
		var _resize = {
			init: function() {
				bp.width = window.innerWidth;
				bp.height = window.innerHeight;

				// STICKY FOOTER
				var header = document.querySelector('header'),
				footer = document.querySelector('footer'),
				headerHeight = 0,
				footerHeight;

				if(header) {
					 headerHeight = header.offsetHeight;
				}
				if(footer) {
					footerHeight = footer.offsetHeight;
					footer.style.marginTop = -(footerHeight) + 'px';
					document.querySelector('#main-wrapper').style.paddingBottom = footerHeight + 'px';
				}

				// for equal height
				bp.equalize(document.querySelectorAll('.fl'));
			}
		}
		_resize.init();
	},
	equalize: function(target) {
		for (var i = 0; i < target.length; i++) {
			target[i].style.minHeight = 0;
		}

		var _biggest = 0;
		for (var i = 0; i < target.length; i++ ){
			var element_height = target[i].offsetHeight;
			if(element_height > _biggest ) _biggest = element_height;
		}

		for (var i = 0; i < target.length; i++) {
			target[i].style.minHeight = _biggest + 'px';
		}
		
		return _biggest;
	},
	mobileDetect: function() {
		var isMobile = {
		    Android: function() {
		        return navigator.userAgent.match(/Android/i);
		    },
		    BlackBerry: function() {
		        return navigator.userAgent.match(/BlackBerry/i);
		    },
		    iOS: function() {
		        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		    },
		    Opera: function() {
		        return navigator.userAgent.match(/Opera Mini/i);
		    },
		    Windows: function() {
		        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
		    },
		    any: function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		    }
		};
		if(isMobile.any) {
			bp.isMobile = isMobile.any();
		}
	},
	videoGallery: function(opt) {
		var _video = {
			target: '',
			index: 0,
			width: 0,
			height: 0,
			baseWidth: 1280,
			baseHeight: 720,
			items: '',
			player: '',
			iframe: '',
			init: function() {
				_video.target = opt.target;
				_video.index = opt.index;

				_video.items = _video.target.querySelectorAll('.cv-li-set');
				_video.player = _video.target.querySelector('.co-vi-player');

				for (var i = 0; i < _video.items.length; i++) {
					var _this = _video.items[i];
					_this.setAttribute('video-index', i);

					_this.addEventListener('click', function(e) {
						e.preventDefault();

						if(!(this.classList.contains('selected'))) {
							_video.create(this);
						}
					});
				}

				_video.resize();

				window.addEventListener('resize', function() {
					_video.resize();

					clearTimeout(_video.resizeTimer);
					_video.resizeTimer = setTimeout(function() {
						_video.resize();
					}, 400);
				});
			},
			create: function(target) {
				_video.player.innerHTML = '';

				var _playerID = 'video-player' + _video.index + Math.floor(target.getAttribute('video-index')),
				_videoID = target.getAttribute('href').split('v=');

				_video.iframe = document.createElement('div');
				_video.iframe.setAttribute('id', _playerID);
				_video.player.appendChild(_video.iframe);

				var player = new YT.Player(_playerID, {
					width: '100%',
					height: '100%',
					videoId: _videoID[1],
					playerVars: {
						autoplay: 1,
						showinfo: 0,
						controls: 0,
						enablejsapi: 1,
						loop: 1,
						rel: 0
					},
					events: {
						onReady: function(event) {
							event.target.setPlaybackQuality('hd1080');
						},
						onStateChange: function(event) {
							if (event.data == YT.PlayerState.BUFFERING) {
								event.target.setPlaybackQuality('hd1080');
							}
							if(event.data === YT.PlayerState.ENDED){
								event.target.seekTo(0);
								event.target.pauseVideo();
								console.log(event.target);
							}
						}
					}
				});

				// change the active item
				for (var i = 0; i < _video.items.length; i++) {
					_video.items[i].classList.remove('selected');
				}
				target.classList.add('selected');
			},
			resizeTimer: 0,
			resize: function() {
				_video.width = _video.player.offsetWidth;
				_video.height = _video.baseHeight * (_video.player.offsetWidth / _video.baseWidth);
				_video.player.style.height = _video.height + 'px';
			}
		}
		_video.init();
	}
}
bp.init();

document.addEventListener('DOMContentLoaded', function() {
	bp.ready();
});

window.addEventListener('resize', function() {
	bp.resize();

	clearTimeout(bp.resizeTimer);
	bp.resizeTimer = setTimeout(function() {
		bp.resize();
	}, 400);
});

window.onYouTubeIframeAPIReady = function() {
	console.log('youtube ready');

	var _comVideo = document.querySelectorAll('.com_video');

	for (var i = 0; i < _comVideo.length; i++) {
		bp.videoGallery({
			target: _comVideo[i],
			index: i
		});
	}
}