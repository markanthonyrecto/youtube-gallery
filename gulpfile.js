'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var htmlmin = require('gulp-htmlmin');

gulp.task('browserSync', function() {
	browserSync.init({server: {baseDir: 'public'}})
});

// compile sass
gulp.task('styles', function(){
	return gulp.src('source/scss/**/*.scss')
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(autoprefixer({'overrideBrowserslist': ['last 2 versions'], cascade: false}))
		.pipe(gulp.dest('public/assets/css'))
		.pipe(browserSync.reload({stream: true}))
});

// compile & compress js
gulp.task('scripts', function() {
	gulp.src([
		// 'source/js/lib/jquery.min.js',
		// 'source/js/lib/jquery-ui.min.js',
		'source/js/custom.js'
	])
	.pipe(concat('custom.js'))
	// .pipe(uglify('custom.js')) // turn this off if dont need to minify the js
	.pipe(gulp.dest('public/assets/js'));
});

// minify html
gulp.task('html', function() {
	return gulp.src('source/**/*.html')
	.pipe(htmlmin({collapseWhitespace: false, removeComments: true}))
    .pipe(gulp.dest('public'));
});

gulp.task('watch', ['browserSync', 'styles', 'scripts', 'html'], function() {
	gulp.watch('source/scss/**/*.scss', ['styles']);
	gulp.watch('source/js/**/*.js', ['scripts', browserSync.reload]);
	gulp.watch('public/**/*.html', ['html', browserSync.reload]);
	gulp.watch('source/**/*.html', ['html', browserSync.reload]);
});

gulp.task('default', ['watch']);